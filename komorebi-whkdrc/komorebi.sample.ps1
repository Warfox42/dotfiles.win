if (!(Get-Process whkd -ErrorAction SilentlyContinue))
{
    Start-Process whkd -WindowStyle hidden
}

. $PSScriptRoot\komorebi.generated.ps1

# Send the ALT key whenever changing focus to force focus changes
komorebic alt-focus-hack enable
# Default to minimizing windows when switching workspaces
komorebic window-hiding-behaviour cloak
# Set cross-monitor move behaviour to insert instead of swap
komorebic cross-monitor-move-behaviour insert
# Enable hot reloading of changes to this file
komorebic watch-configuration enable

# create named workspaces I-V on monitor 0
komorebic ensure-named-workspaces 0 A B C D E F
komorebic ensure-named-workspaces 1 I II III IV V

# assign layouts to workspaces, possible values: bsp, columns, rows, vertical-stack, horizontal-stack, ultrawide-vertical-stack
komorebic named-workspace-layout A bsp
komorebic named-workspace-layout B ultrawide-vertical-stack
komorebic named-workspace-layout C bsp
komorebic named-workspace-layout D horizontal-stack

komorebic named-workspace-layout I bsp
komorebic named-workspace-layout II ultrawide-vertical-stack
komorebic named-workspace-layout III bsp
komorebic named-workspace-layout IV horizontal-stack

# set the gaps around the edge of the screen for a workspace
komorebic named-workspace-padding A 15
komorebic named-workspace-padding B 15
komorebic named-workspace-padding C 175
komorebic named-workspace-padding D 15

komorebic named-workspace-padding I 15
komorebic named-workspace-padding II 15
komorebic named-workspace-padding III 175
komorebic named-workspace-padding IV 15

# set the gaps between the containers for a workspace
komorebic named-workspace-container-padding A 5
komorebic named-workspace-container-padding B 5
komorebic named-workspace-container-padding C 5
komorebic named-workspace-container-padding D 5

komorebic named-workspace-container-padding I 5
komorebic named-workspace-container-padding II 5
komorebic named-workspace-container-padding III 5
komorebic named-workspace-container-padding IV 5

# you can assign specific apps to named workspaces
# komorebic named-workspace-rule exe "Firefox.exe" C

# Configure the invisible border dimensions
komorebic invisible-borders 7 0 14 7

# Uncomment the next lines if you want a visual border around the active window
# komorebic active-window-border-colour 66 165 245 --window-kind single
# komorebic active-window-border-colour 256 165 66 --window-kind stack
# komorebic active-window-border-colour 255 51 153 --window-kind monocle
# komorebic active-window-border enable

komorebic complete-configuration