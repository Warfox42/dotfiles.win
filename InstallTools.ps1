# Install config

# Checking access rights
Set-ExecutionPolicy Bypass -Scope Process -Force;
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072;

# Downloading and installing the package manager
irm get.scoop.sh -outfile 'install.ps1';
iex "& {$(irm get.scoop.sh)} -RunAsAdmin";

# Installing the necessary programs using the packages manager scoop
scoop install git aria2;
scoop bucket add extras;
scoop install komorebi@0.1.17;
scoop install windows-terminal@1.18.3181.0;
scoop install whkd neovim gcc;

# Creating folders
mkdir $Env:USERPROFILE\.config -ea 0;

# Downloading config files
iwr https://gitlab.com/Warfox42/dotfiles.win/-/raw/main/komorebi-whkdrc/komorebi.generated.sample.ps1?ref_type=heads -OutFile $Env:USERPROFILE\komorebi.generated.ps1;
iwr https://gitlab.com/Warfox42/dotfiles.win/-/raw/main/komorebi-whkdrc/komorebi.sample.ps1?ref_type=heads -OutFile $Env:USERPROFILE\komorebi.ps1;
iwr https://gitlab.com/Warfox42/dotfiles.win/-/raw/main/komorebi-whkdrc/whkdrc.sample?ref_type=heads -OutFile $Env:USERPROFILE\.config\whkdrc;
iwr https://gitlab.com/Warfox42/dotfiles.win/-/raw/main/WindowsTerminal/settings.sample.json?ref_type=heads -OutFile "$Env:USERPROFILE\scoop\apps\windows-terminal\current\settings\settings.json";

# Run komorebi WM
komorebic start --await-configuration
