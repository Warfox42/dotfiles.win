# First install script 
The script installs the following programs: 
- Scoop - Package manager for Windows
- 7zip - Archiver
- Git - Distributed Version Control System (DVCS)
- Aria2c - Download Manager
- Komorebi + _config_ - Window Manager for Windows (Version 0.1.17)
- Whkd + _config_ - AutoHotKey for Windows (Version 0.2.0)
- Windows Terminal + _config_ - Simple terminal (Version 1.18.3181.0)
- GCC Compiler - GNU Compiler Collection
- NeoVim - Text Editor (Version 0.9.5)

To start the script, you need the following command:
```powershell
Set-ExecutionPolicy Bypass -Scop Process -Force;iwr https://gitlab.com/Warfox42/dotfiles.win/-/raw/main/InstallTools.ps1?ref_type=heads -OutFile '.\InstallTools.ps1'; & "~\InstallTools.ps1";if ((Test-Path -Path "~\InstallTools.ps1") -and (Test-Path -Path "~\install.ps1")) {Remove-Item "~\InstallTools.ps1";Remove-Item "~\install.ps1"}
```
